import cv2 as cv
import numpy as np


#Метод Собеля
def sobelF(grey_img):
    edges_x = cv.Sobel(grey_img, cv.CV_16S, 1, 0)
    edges_y = cv.Sobel(grey_img, cv.CV_16S, 0, 1)

    edges_x = cv.convertScaleAbs(edges_x)
    edges_y = cv.convertScaleAbs(edges_y)
    edges = cv.addWeighted(edges_x, 0.5, edges_y, 0.5, 0)
    cv.imshow('Sobel', edges)


#Метод Робертса
def robertsF(grey_img):
    kernelx = np.array([[-1, 0], [0, 1]], dtype=int)
    kernely = np.array([[0, -1], [1, 0]], dtype=int)
    edges_x = cv.filter2D(grey_img, cv.CV_16S, kernelx)
    edges_y = cv.filter2D(grey_img, cv.CV_16S, kernely)

    edges_x = cv.convertScaleAbs(edges_x)
    edges_y = cv.convertScaleAbs(edges_y)
    edges = cv.addWeighted(edges_x, 0.5, edges_y, 0.5, 0)

    cv.imshow('Roberts', edges)


#Метод Лапласа
def laplasF(grey_img):
    dst = cv.Laplacian(grey_img, ddepth, ksize=kernel_size)
    abs_dst = cv.convertScaleAbs(dst)

    cv.imshow('Laplacian', abs_dst)

#Метод Канни
def cannyF(grey_img):
    edges = cv.Canny(grey_img, 100, 200)

    cv.imshow('Canny', edges)


if __name__ == '__main__':
    # Открыть видео
    video_src = 'video.mp4'
    ddepth = cv.CV_16S
    kernel_size = 3
    cap = cv.VideoCapture(video_src)
    ret, img = cap.read()
    h, w = img.shape[:2]

    img = cv.GaussianBlur(img, (3, 3), 0)
    img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    grey_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    cv.imshow('Original Image', img)
    cv.imshow('Grey', grey_img)

    sobelF(grey_img)
    robertsF(grey_img)
    laplasF(grey_img)
    cannyF(grey_img)

    cv.waitKey(0)
